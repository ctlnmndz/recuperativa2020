#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Se importan las clases
from clasemotorparte2 import Motor  
from claseVelocimetroParte2 import Velocimetro
from claseBateriaParte2 import Bateria
from claseRuedasparte2 import Rueda

if __name__ == '__main__':
    # variables a utilizar
    ruedas = 4
    recorrido = 0
    motor = 0
    velocimetro = 0
    avanzar = 0
    bateria = 400
    recorrido_rueda = 0
    
    print("*Automóvil eléctrico, nuestro auto está cargado al máximo*")
    avanzar = int(input("Si desea avazar ingrese 1 de lo contario "
                        "ingrese 0\n"))
    if (avanzar==1):
        
        while(avanzar == 1):
           
            # Motor
            motor = int(input("Si desea encender el motor ingrese 1, de" 
                              " lo contrario ingrese 0\n"))
            if(motor == 1):
                # Se instancia la clase
                encender = Motor(motor)
                encender.set_motor(motor)
                
                # Velocimetro
                velocimetro = int(input("ingrese la velocidad del auto:\n"))
                #  se instancia la clase
                velocidad = Velocimetro(velocimetro)
                velocidad.set_velocimetro(velocimetro)
               
                # Batería
                recorrido = int(input("Indique la distancia recorrida:\n"))
                # Se instancia la clase
                cantidad_bateria = Bateria(bateria, recorrido)
                # Si hay batería el auto se movera 
                if (bateria >= 0):
                    bateria1 = (recorrido*400)/200
                    print("Se ha agotado un", bateria1 ,"Wh/kg de la "
                          "bateria")
                    bateria = bateria - bateria1
                    bateria = bateria
                    # Si la cantidad de bateria es menor a la requerida
                    if(bateria < 0):
                        print("EL auto no puede recorrer esta distancia"
                              " ya que no queda batería suficiente")
                        print("La batería disponible es de:", bateria)
                    else:          
                        print("Lo que queda de batería es: ", 
                              bateria ,"Wh/kg")

    
                    #Ruedas
                    recorrido_rueda = recorrido + recorrido_rueda
                    # Se instancia la clase
                    rueda = Rueda(ruedas, recorrido_rueda)
                    rueda.set_rueda(recorrido_rueda)
                    
                    # Se vuelve a preguntar al usuario si quiere avanzar
                    avanzar = int(input("Si desea volver avazar ingrese" 
                                       " 1 de lo contario ingrese 0\n"))
                # cuando no quedé bateria se termina el programa
                if(bateria == 0):
                    print("La batería se ha agotado, ya no puede avanzar")
                    print("Gracias por usar nuetro auto")
                    avanzar = 0
            # si no enciende el motor se instancia sólo la clase motor
            else:
                encender = Motor(motor)
                encender.set_motor(motor)
    # Si no desea moverse
    else:
        print("El auto no se moverá")        

