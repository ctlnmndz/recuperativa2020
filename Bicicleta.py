#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import random

class Bicicleta:
    def __init__(self):
        self.__velocidad = 0
        self.__factor_suerte = 0

    def set_velocidad(self, velocidad):
        self.__velocidad = velocidad


    def get_velocidad(self):
        return self.__velocidad


    # factor suerte de los pilotos al azar
    def set_factor_suerte(self):
        self.__factor_suerte = random.randint(1,10)


    def get_factor_suerte(self):
        return self.__factor_suerte
