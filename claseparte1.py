#!/usr/bin/env python3
# -*- coding: utf-8 -*-


class determinar_ganador:

    # Constructor de la funcion
    def __init__(self, distancia_pista, distancia_MTB, 
                 distancia_paseo, distancia_carrera):
                     
        self._distancia_pista = distancia_pista
        self._distancia_MTB = distancia_MTB
        self._distancia_paseo = distancia_paseo
        self._distancia_carrea = distancia_carrera
     
    # Se ve quién avanzó más en la carrera
    def set_mayor_diferencia(self, distancia_pista, distancia_MTB, 
                             distancia_paseo, distancia_carrera):
        
        # Se comparan las variables y se determina el ganador
        if(distancia_pista > distancia_paseo and 
           distancia_pista > distancia_MTB):
            
            if(distancia_pista > distancia_carrera):
                print("El ganador es el tipo de bicicleta Pista")
           
            else:
                print("Ninguno cruzó la meta, pero el que estuvo más " 
                      "cerca de ella fue el tipo de bicicleta Pista")
        
        if(distancia_MTB > distancia_paseo and 
           distancia_MTB > distancia_pista):
            
            if(distancia_MTB > distancia_carrera):
                print("El ganador es el tipo de bicicleta MTB")
           
            else:
                print("Ninguno cruzó la meta, pero el que estuvo más " 
                      "cerca de ella fue el tipo de bicicleta MTB")
       
        if(distancia_paseo > distancia_pista and 
           distancia_paseo > distancia_MTB):
            
            if(distancia_paseo > distancia_carrera):
                print("El ganador es el tipo de bicicleta paseo")
            
            else:
                print("Ninguno cruzó la meta, pero el que estuvo más " 
                      "cerca de ella fue el tipo de bicicleta paseo")

         
        
        
        
        


    
    
