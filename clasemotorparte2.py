#!/usr/bin/env python
# -*- coding: utf-8 -*-

class Motor:
   
    def __init__(self, motor):
        self._motor = motor
    
    def set_motor(self, motor):
        # si el usuario enciende el motor.
        if( motor == 1):
            print("El auto avanzará en linea recta")
        # Si el usuario no enciende el motor.
        else:
            print("El auto no avanzará, ya que el motor esta apagado")
    
    
