#!/usr/bin/env python
# -*- coding: utf-8 -*-

class Rueda:
   
    def __init__(self, ruedas, recorrido_rueda):
        self._ruedas = ruedas 
        self._recorrido_rueda = recorrido_rueda 
    
    def set_rueda(self, recorrido_rueda):
        # Si el recorrido de las ruedas es mayor a 250 km
        if( recorrido_rueda >= 250):
            print("Le recomendamos hacer un cambio de ruedas al auto")
        # Si el recorrido es menor a 250 km
        else:
            print("El estado de las ruedas estan intactas")
