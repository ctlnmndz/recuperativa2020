#!/usr/bin/env python
# -*- coding: utf-8 -*-

class Velocimetro:
    def __init__(self, velocimetro):
        self._velocimetro = velocimetro

    def set_velocimetro(self, velocimetro):
        # Si la velocidad excede la velocidad máxima
        if(velocimetro > 150):
            print("Usted esta excediendo la velocidad máxima"
                  ", porfavor reducirla")
            print("Su velocidad es de:", velocimetro, "km/h")
            
        # Si la velocidad es normal    
        else: 
            print("Usted esta en una velocidad permitida")
            print("Su velocidad es de:", velocimetro, "km/h")

        
