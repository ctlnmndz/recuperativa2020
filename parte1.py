#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import random
from claseparte1 import determinar_ganador
from Bicicleta import Bicicleta
from Bici_Pista import Pista
from Bici_Paseo import Pista
from Bici_MTB import Pista


if __name__ == '__main__':

    # Crear bicicletas    
    bici_pista = Bicicleta()
    bici_paseo = Bicicleta()
    bici_MTB = Bicicleta()

    # Valores caracteristicas bicicletas
    bici_pista.set_velocidad(20)
    bici_pista.set_factor_suerte()

    bici_paseo.set_velocidad(15)
    bici_paseo.set_factor_suerte()

    bici_MTB.set_velocidad(10)
    bici_MTB.set_factor_suerte()

    print("Se imprime el factor suerte del piloto pista", bici_pista.get_factor_suerte())
    print("Se imprimer el factor suerte del piloto Mtb ", bici_MTB.get_factor_suerte())
    print("Se imprimer el factor suerte del piloto paseo", bici_paseo.get_factor_suerte())
    
    # Se intercatua con el usuario
    distancia_carrera = float(input("¿Cúal es la distancia a recorrer en la car"
                            "rera? Solo factor sin unidad de medida: "))
    
    # Distancia recorrida por las bicicletas
    distancia_pista = (bici_pista.get_velocidad()*bici_pista.get_factor_suerte())/distancia_carrera
    distancia_MTB = (bici_MTB.get_velocidad()*bici_MTB.get_factor_suerte())/distancia_carrera
    distancia_paseo = (bici_paseo.get_velocidad()*bici_paseo.get_factor_suerte())/distancia_carrera
    
    print("Se imprime distancia avanzada bicicleta pista",
          distancia_pista)
    print("Se imprime distancia avanzada bicicleta Mtb",
          distancia_MTB)
    print("Se imprime distancia avanzada bicicleta paseo",
          distancia_paseo)
    
    ganador = determinar_ganador(distancia_pista, distancia_MTB, 
                                 distancia_paseo, distancia_carrera)
    
    ganador.set_mayor_diferencia(distancia_pista, distancia_MTB, 
                                 distancia_paseo, distancia_carrera)
    
    
    
    
    
    
    
    
